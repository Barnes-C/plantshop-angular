= Entwicklerhandbuch
Gruppe 2 - Software Engineering
:doctype: book
:icons: font
:plantuml-server-url: http://www.plantuml.com/plantuml

IMPORTANT: Copyright (C) 2021 - present Christopher Barnes, Lasse Linnamäki, Melina Wünsch, Tobias Weber, Hochschule Karlsruhe. +
           Free use of this software is granted under the terms of the
           GNU General Public License (GPL) v3. +
           For the full text of the license, see the http://www.gnu.org/licenses/gpl-3.0.html[license] file.

== Zustandsdiagramm

=== Das Zustandsdiagramm im Überblick

Zu allererst gibt ein Kunde oder Mitarbeiter Suchkriterien für eine bestimmte Pflanze ein, die er oder sie als Ergebnis zurückbekommen möchte. Durch einen Klick auf den Submit-Button wird durch @ViewChild eine Referenz der Subkomponente in die Elternkomponente injiziert. Dies kann auch durch HTML-Tags in der Elternkomponente geschehen. Dadurch müssen die Tags den Selektoren der Subkomponenten entsprechen. 
Die Suchkriterien, als Output des Suchformulars, gehen in SuchePflanze ein. Abschließend sind die Suchkriterien der Input für das Suchergebnis, das dann die gewünschte oder die gewünschten Pflanzen ausgibt.

---

image::Zustandsdiagramm.svg[opts=inline, width=75%]

© Linnamäki, Barnes, Wünsch, Weber

---