export enum HttpStatus {
    OK = 200,
    NOT_FOUND = 404,
    TOO_MANY_REQUESTS = 429,
}
