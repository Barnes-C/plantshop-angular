import { CommonModule } from '@angular/common';
import { ErrorMessageComponent } from './error-message.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [ErrorMessageComponent],
    exports: [ErrorMessageComponent],
    imports: [CommonModule],
})
export class ErrorMessageModule {}
