// Charts mittes JavaScript, siehe http://www.jsgraphs.com und
// http://jqueryhouse.com/javascript-chart-and-graph-libraries
// - D3: fuehrend, flexibel, aber keine vorgefertigten Layouts fuer z.B.
// Balken-Diagramme
// - Google Charts: nur online benutzbar, JS-Datei *nicht* auf eigenem Server
// benutzbar
// - Chart.js 250.000 Downloads/Monat
// - NVD3 basiert auf D3, 40.000 Downloads/Monat
// - Power BI Visuals https://github.com/Microsoft/PowerBI-visuals
// - ...

import { Chart } from 'chart.js';
import type { ChartConfiguration } from 'chart.js';
import { Injectable } from '@angular/core';

interface ColorHighlight {
    color: string;
    highlight: string;
}

/**
 * Service-Klasse f&uuml;r die Verwendung von Chart.js.
 */
// http://blog.thoughtram.io/angular/2015/09/17/resolve-service-dependencies-in-angular-2.html
// http://blog.thoughtram.io/angular/2015/09/03/forward-references-in-angular-2.html
@Injectable({ providedIn: 'root' })
export class DiagrammService {
    private static readonly DEFAULT_COLOR = '#F7464A';

    private static readonly DEFAULT_HIGHLIGHT_COLOR = '#F7464A';

    private readonly backgroundColors = new Map<number, ColorHighlight>();

    constructor() {
        // red
        this.backgroundColors.set(0, {
            color: DiagrammService.DEFAULT_COLOR,
            highlight: DiagrammService.DEFAULT_HIGHLIGHT_COLOR,
        });
        // green
        this.backgroundColors.set(1, {
            color: '#46BFBD',
            highlight: '#5AD3D1',
        });
        // yellow
        this.backgroundColors.set(2, {
            color: '#FDB45C',
            highlight: '#FFC870',
        });

        console.log(
            'DiagrammService.constructor(): backgroundColors=',
            this.backgroundColors,
        );
    }

    /**
     * @param elementId ID des HTML-Tags, bei dem das Chart eingesetzt wird.
     * @return Chart-Objekt
     */
    createChart(
        chartElement: HTMLCanvasElement | undefined,
        config: ChartConfiguration,
    ): Chart | undefined {
        if (chartElement === undefined) {
            console.error(
                'DiagrammService.createChart(): Kein HTML-Element fuer ein Chart gefunden:',
                chartElement,
            );
            return;
        }

        const context = chartElement.getContext('2d');
        if (context === null) {
            console.error(
                'DiagrammService.createChart(): Kein 2D-Kontext gefunden',
                context,
            );
            return;
        }

        console.log(
            'DiagrammService.createChart(): Chart wird erzeugt:',
            context,
            config,
        );
        return new Chart(context, config);
    }

    /**
     * @param idx Fortlaufende Nummer f&uuml;r die Farbe bei einem
     *        Tortendiagramm.
     * @return String mit dem Hex-Code der Farbe.
     */
    getBackgroundColor(idx: number) {
        const colorHighlight = this.backgroundColors.get(idx % 3);
        return colorHighlight === undefined
            ? DiagrammService.DEFAULT_COLOR
            : colorHighlight.color;
    }

    /**
     * @param idx Fortlaufende Nummer f&uuml;r die Farbe zur Hervorhebung bei
     *        einem Tortendiagramm.
     * @return String mit dem Hex-Code dieser Farbe.
     */
    getHoverBackgroundColor(idx: number) {
        const backgroundColor = this.backgroundColors.get(idx % 3);
        return backgroundColor === undefined
            ? DiagrammService.DEFAULT_HIGHLIGHT_COLOR
            : backgroundColor.highlight;
    }
}
