import { Component } from '@angular/core';

@Component({
    selector: 'hs-waiting',
    templateUrl: './waiting.component.html',
})
export class WaitingComponent {
    constructor() {
        console.log('WaitingComponent.constructor()');
    }
}
