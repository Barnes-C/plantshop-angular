import { CookieService } from './cookie.service';
import { Injectable } from '@angular/core';
// import { JwtService } from './jwt.service';
import { Subject } from 'rxjs';
// eslint-disable-next-line sort-imports
import { BasicAuthService } from './basic-auth.service';

export const ROLLE_ADMIN = 'ROLE_ADMIN';
// Spring Security:
// export const ROLLE_ADMIN = 'ROLE_ADMIN'

/* eslint-disable no-underscore-dangle */
@Injectable({ providedIn: 'root' })
export class AuthService {
    // Subject statt Observable:
    // in login() und logout() wird Subject.next() aufgerufen
    private readonly _isLoggedInSubject = new Subject<boolean>();

    private readonly _rollenSubject = new Subject<Array<string>>();

    constructor(
        private readonly basicAuthService: BasicAuthService,
        // private readonly jwtService: JwtService,
        private readonly cookieService: CookieService,
    ) {
        console.log('AuthService.constructor()');
    }

    /**
     * @param username als String
     * @param password als String
     * @return void
     */
    async login(username: string | undefined, password: string | undefined) {
        console.log(
            `AuthService.login(): username=${username}, password=${password}`,
        );
        // eslint-disable-next-line prefer-const
        let rollen: Array<string> = [];
        try {
            // this.basicAuthService.login(username, password);
            rollen[0] = await this.basicAuthService.login(username, password);
            console.log('AuthService.login()', rollen);
            this.isLoggedInSubject.next(true);
        } catch (error) {
            console.warn('AuthService.login(): Exception', error);
            this.isLoggedInSubject.next(false);
        }

        this.rollenSubject.next(rollen);
    }

    /**
     * @return void
     */
    logout() {
        console.warn('AuthService.logout()');
        this.cookieService.deleteAuthorization();
        this.isLoggedInSubject.next(false);
        this.rollenSubject.next([]);
    }

    get isLoggedInSubject() {
        return this._isLoggedInSubject;
    }

    get rollenSubject() {
        return this._rollenSubject;
    }

    /**
     * @return String fuer JWT oder Basic-Authentifizierung
     */
    get authorization() {
        return this.cookieService.getAuthorization();
    }

    /**
     * @return true, falls ein User eingeloggt ist; sonst false.
     */
    get isLoggedIn() {
        return this.cookieService.getAuthorization() !== undefined;
    }

    /**
     * @return true, falls ein User in der Rolle "admin" eingeloggt ist;
     *         sonst false.
     */
    get isAdmin() {
        // z.B. 'admin,mitarbeiter'
        const rolesString = this.cookieService.getRoles();
        if (rolesString === undefined) {
            return false;
        }

        // z.B. ['admin', 'mitarbeiter']
        const rolesArray = rolesString.split(',');
        return rolesArray !== undefined && rolesArray.includes(ROLLE_ADMIN);
    }
}

/* eslint-enable no-underscore-dangle */
