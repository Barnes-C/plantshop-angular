/* eslint-disable no-undef */
import type { ComponentFixture } from '@angular/core/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from './home.component';
import { MaterialModule } from '../material.module';

// fixture enables the access to DOM-Elements and listenes to them for changes.
// TestBed configures and initializes environment for unit testing and provides methods for creating components and services in unit tests.

// -----------------------------------------------------------------------------
// T e s t s
// -----------------------------------------------------------------------------
// eslint-disable-next-line no-undef
describe('HomeComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [HomeComponent],
        }).compileComponents();
    });

    // Helps Debugging Tests and tracks runtime changes
    let fixture: ComponentFixture<HomeComponent>;
    let homeComponent: HomeComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [HomeComponent],
            imports: [RouterTestingModule, MaterialModule],
        }).compileComponents();
    });
    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        homeComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    afterEach(() => {
        fixture.destroy();
    });

    it('should create the home', () => {
        const fixture = TestBed.createComponent(HomeComponent);
        const home = fixture.componentInstance;
        expect(home).toBeTruthy();
    });

    it(`should have as headline (h1) 'Hello, welcome to the PlantShop!'`, () => {
        const fixture = TestBed.createComponent(HomeComponent);
        const home = fixture.nativeElement;
        expect(home.querySelector('#headerHome').textContent).toEqual(
            ' Hello, welcome to the PlantShop!\n',
        );
    });

    it('should render title "PlantShop home"', () => {
        const fixture = TestBed.createComponent(HomeComponent);
        fixture.detectChanges();
        const home = fixture.nativeElement;
        expect(home.querySelector('title').textContent).toContain(
            'PlantShop home',
        );
    });
});
