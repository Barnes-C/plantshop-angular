import { Component } from '@angular/core';
import type { OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'hs-home',
    // https://next.angular.io/guide/i18n
    // https://angular.io/guide/i18n
    // https://github.com/angular/angular/tree/master/packages/common/locales
    // http://cldr.unicode.org
    templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
    constructor(private readonly title: Title) {
        console.log('HomeComponent.constructor()');
    }

    ngOnInit() {
        this.title.setTitle('PlantShop');
    }
}
