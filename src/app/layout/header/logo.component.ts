import { Component } from '@angular/core';

/**
 * Komponente f&uuml;r das Logo mit dem Tag &lt;hs-logo&gt;.
 */
@Component({
    selector: 'hs-logo',
    templateUrl: './logo.component.html',
})
export class LogoComponent {
    constructor() {
        console.log('LogoComponent.constructor()');
    }
}
