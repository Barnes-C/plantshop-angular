// Direktiven (z.B. ngFor, ngIf) und Pipes
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginLogoutComponent } from './login-logout.component';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../../material.module';

@NgModule({
    declarations: [LoginLogoutComponent],
    exports: [LoginLogoutComponent],
    imports: [CommonModule, FormsModule, MaterialModule],
})
export class LoginLogoutModule {}
