import { HeaderComponent } from './header.component';
import { LogoModule } from './logo.module';
import { NavModule } from './nav.module';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../../material.module';

// single component module

@NgModule({
    declarations: [HeaderComponent],
    exports: [HeaderComponent],
    imports: [LogoModule, NavModule, MaterialModule],
})
export class HeaderModule {}
