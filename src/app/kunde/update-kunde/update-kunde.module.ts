import { CommonModule } from '@angular/common';
import { ErrorMessageModule } from '../../shared/error-message.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UpdateBuchComponent } from './update-kunde.component';
import { UpdateStammdatenModule } from './stammdaten/update-stammdaten.module';
import { MaterialModule } from '../../material.module';

@NgModule({
    declarations: [UpdateBuchComponent],
    exports: [UpdateBuchComponent],
    imports: [
        CommonModule,
        FormsModule,
        ErrorMessageModule,
        UpdateStammdatenModule,
        MaterialModule,
    ],
    providers: [Title],
})
export class UpdateBuchModule {}
