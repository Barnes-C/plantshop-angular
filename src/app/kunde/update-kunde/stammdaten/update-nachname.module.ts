import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material.module';

import { UpdateTitelComponent } from './update-nachname.component';

@NgModule({
    declarations: [UpdateTitelComponent],
    exports: [UpdateTitelComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class UpdateTitelModule {}
