import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material.module';

import { UpdateVerlagComponent } from './update-interessen.component';

@NgModule({
    declarations: [UpdateVerlagComponent],
    exports: [UpdateVerlagComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class UpdateVerlagModule {}
