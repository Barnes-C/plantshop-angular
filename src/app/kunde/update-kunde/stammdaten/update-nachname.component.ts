import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-update-nachname</code>
 */
@Component({
    selector: 'hs-update-nachname',
    templateUrl: './update-nachname.component.html',
})
export class UpdateTitelComponent implements OnInit {
    private static readonly MIN_LENGTH = 2;

    // <hs-update-nachname [form]="form" [currentValue]="...">
    @Input()
    readonly form!: FormGroup;

    @Input()
    readonly currentValue!: string;

    nachname!: FormControl;

    ngOnInit() {
        console.log(
            'UpdateTitelComponent.ngOnInit(): currentValue=',
            this.currentValue,
        );
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.nachname = new FormControl(
            this.currentValue,
            Validators.compose([
                Validators.required,
                Validators.minLength(UpdateTitelComponent.MIN_LENGTH),
                Validators.pattern(/^\w.*$/u),
            ]),
        );
        this.form.addControl('nachname', this.nachname);
    }
}
