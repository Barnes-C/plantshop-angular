import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import type { OnInit } from '@angular/core';
// eslint-disable-next-line sort-imports
import type { InteressenType } from '../../shared/kunde';

/**
 * Komponente f&uuml;r das Tag <code>hs-update-interessen</code>
 */
@Component({
    selector: 'hs-update-interessen',
    templateUrl: './update-interessen.component.html',
})
export class UpdateVerlagComponent implements OnInit {
    // <hs-update-interessen [form]="form" [currentValue]="...">
    @Input()
    readonly form!: FormGroup;

    @Input()
    readonly currentValue: InteressenType | undefined | '';

    interessen!: FormControl;

    ngOnInit() {
        console.log(
            'UpdateVerlagComponent.ngOnInit(): currentValue=',
            this.currentValue,
        );
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.interessen = new FormControl(this.currentValue);
        this.form.addControl('interessen', this.interessen);
    }
}
