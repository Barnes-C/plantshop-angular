import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material.module';

import { UpdateRatingComponent } from './update-kategorie.component';

@NgModule({
    declarations: [UpdateRatingComponent],
    exports: [UpdateRatingComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class UpdateRatingModule {}
