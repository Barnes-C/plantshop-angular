/* eslint-disable array-bracket-newline */

import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-update-username</code>
 */
@Component({
    selector: 'hs-update-username',
    templateUrl: './update-username.component.html',
})
export class UpdateIsbnComponent implements OnInit {
    // <hs-update-username [form]="form" [currentValue]="...">
    @Input()
    readonly form!: FormGroup;

    @Input()
    readonly currentValue!: string;

    username!: FormControl;

    ngOnInit() {
        console.log(
            'UpdateIsbnComponent.ngOnInit(): currentValue=',
            this.currentValue,
        );
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.username = new FormControl(this.currentValue, [
            Validators.required,
        ]);
        this.form.addControl('username', this.username);
    }
}
