import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { UpdateArtModule } from './update-geschlecht.module';
import { UpdateIsbnModule } from './update-username.module';
import { UpdateRatingModule } from './update-kategorie.module';
import { UpdateStammdatenComponent } from './update-stammdaten.component';
import { UpdateTitelModule } from './update-nachname.module';
import { UpdateVerlagModule } from './update-interessen.module';
import { MaterialModule } from '../../../material.module';

@NgModule({
    declarations: [UpdateStammdatenComponent],
    exports: [UpdateStammdatenComponent],
    imports: [
        ReactiveFormsModule,
        UpdateArtModule,
        UpdateIsbnModule,
        UpdateRatingModule,
        UpdateTitelModule,
        UpdateVerlagModule,
        MaterialModule,
    ],
})
export class UpdateStammdatenModule {}
