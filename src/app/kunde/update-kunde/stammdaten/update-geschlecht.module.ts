import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material.module';

import { UpdateArtComponent } from './update-geschlecht.component';

@NgModule({
    declarations: [UpdateArtComponent],
    exports: [UpdateArtComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class UpdateArtModule {}
