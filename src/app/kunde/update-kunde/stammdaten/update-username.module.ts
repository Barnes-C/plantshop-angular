import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material.module';

import { UpdateIsbnComponent } from './update-username.component';

@NgModule({
    declarations: [UpdateIsbnComponent],
    exports: [UpdateIsbnComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class UpdateIsbnModule {}
