import { ActivatedRoute, Params } from '@angular/router';
import { BuchService, Kunde } from '../shared';
import type { OnDestroy, OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { HttpStatus } from '../../shared';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

/**
 * Komponente f&uuml;r das Tag <code>hs-update-kunde</code> mit Kindkomponenten
 * f&uuml;r die folgenden Tags:
 * <ul>
 *  <li> <code>hs-stammdaten</code>
 *  <li> <code>hs-schlagwoerter</code>
 * </ul>
 */
@Component({
    selector: 'hs-update-kunde',
    templateUrl: './update-kunde.component.html',
})
export class UpdateBuchComponent implements OnInit, OnDestroy {
    kunde: Kunde | undefined;

    errorMsg: string | undefined;

    private buchSubscription!: Subscription;

    private errorSubscription!: Subscription;

    private idParamSubscription!: Subscription;

    private findByIdSubscription: Subscription | undefined;

    constructor(
        private readonly buchService: BuchService,
        private readonly titleService: Title,
        private readonly route: ActivatedRoute,
    ) {
        console.log('UpdateBuchComponent.constructor()');
    }

    ngOnInit() {
        // Die Beobachtung starten, ob es ein zu aktualisierendes Kunde oder
        // einen Fehler gibt.
        this.buchSubscription = this.subscribeBuch();
        this.errorSubscription = this.subscribeError();

        // Pfad-Parameter aus /kunden/:id/update
        this.idParamSubscription = this.subscribeIdParam();

        this.titleService.setTitle('Aktualisieren');
    }

    ngOnDestroy() {
        this.buchSubscription.unsubscribe();
        this.errorSubscription.unsubscribe();
        this.idParamSubscription.unsubscribe();

        if (this.findByIdSubscription !== undefined) {
            this.findByIdSubscription.unsubscribe();
        }
    }

    private subscribeBuch() {
        const next = (kunde: Kunde) => {
            this.errorMsg = undefined;
            this.kunde = kunde;
            console.log('UpdateBuch.kunde=', this.kunde);
        };
        return this.buchService.buchSubject.subscribe(next);
    }

    /**
     * Beobachten, ob es einen Fehler gibt.
     */
    private subscribeError() {
        const next = (error: string | number | undefined) => {
            this.kunde = undefined;

            if (error === undefined) {
                this.errorMsg = 'Ein Fehler ist aufgetreten.';
                return;
            }

            if (typeof error === 'string') {
                this.errorMsg = error;
                return;
            }

            switch (error) {
                case HttpStatus.NOT_FOUND:
                    this.errorMsg = 'Kein Kunde vorhanden.';
                    break;
                default:
                    this.errorMsg = 'Ein Fehler ist aufgetreten.';
                    break;
            }
            console.log(`UpdateBuchComponent.errorMsg: ${this.errorMsg}`);
        };

        return this.buchService.errorSubject.subscribe(next);
    }

    private subscribeIdParam() {
        const next = (parameters: Params) => {
            console.log('params=', parameters);
            this.findByIdSubscription = this.buchService.findById(
                parameters.id,
            );
        };
        // ActivatedRoute.params is an Observable
        return this.route.params.subscribe(next);
    }
}
