/* eslint-disable max-lines */

const MIN_RATING = 0;
const MAX_RATING = 6;

export enum InteressenType {
    S = 'SPORT',
    L = 'LESEN',
    R = 'REISEN',
}

export enum GeschlechtType {
    M = 'MAENNLICH',
    W = 'WEIBLICH',
    D = 'DIVERS',
}

export enum FamilienstandType {
    VH = 'VERHEIRATET',
    L = 'LEDIG',
    G = 'GESCHIEDEN',
    VW = 'VERWITTWET',
}

/**
 * Gemeinsame Datenfelder unabh&auml;ngig, ob die Buchdaten von einem Server
 * (z.B. RESTful Web Service) oder von einem Formular kommen.
 */
export interface Umsatz {
    betrag: Int16Array;
    waehrung: string;
}

export interface Adresse {
    ort: string;
    plz: string;
}

export interface User {
    username: string;
    password: string;
}

export interface KundeShared {
    _id?: string;
    nachname: string;
    kategorie: number;
    geschlecht: GeschlechtType;
    interessen: Array<InteressenType>;
    geburtsdatum: Date;
    umsatz: Umsatz;
    newsletter?: boolean;
    username: string;
    version?: number;
    user: User;
    email: string;
    adresse: Adresse;
    homepage: string;
    familienstand: FamilienstandType;
}

interface Link {
    href: string;
}

/**
 * Daten vom und zum REST-Server:
 * <ul>
 *  <li> Arrays f&uuml;r mehrere Werte, die in einem Formular als Checkbox
 *       dargestellt werden.
 *  <li> Daten mit Zahlen als Datentyp, die in einem Formular nur als
 *       String handhabbar sind.
 * </ul>
 */
export interface KundeServer extends KundeShared {
    _id?: string;
    nachname: string;
    kategorie: number;
    geschlecht: GeschlechtType;
    interessen: Array<InteressenType>;
    geburtsdatum: Date;
    umsatz: Umsatz;
    newsletter?: boolean;
    username: string;
    version?: number;
    user: User;
    email: string;
    adresse: Adresse;
    homepage: string;
    familienstand: FamilienstandType;
    _links?: {
        self: Link;
        list?: Link;
        add?: Link;
        update?: Link;
        remove?: Link;
    };
}

/**
 * Daten aus einem Formular:
 * <ul>
 *  <li> je 1 Control fuer jede Checkbox und
 *  <li> au&szlig;erdem Strings f&uuml;r Eingabefelder f&uuml;r Zahlen.
 * </ul>
 */
export interface KundeForm extends KundeShared {
    S?: boolean;
    L?: boolean;
    R?: boolean;
}

/**
 * Model als Plain-Old-JavaScript-Object (POJO) fuer die Daten *UND*
 * Functions fuer Abfragen und Aenderungen.
 */
export class Kunde {
    private static readonly SPACE = 2;

    ratingArray: Array<boolean> =
        this.kategorie === undefined
            ? new Array(MAX_RATING - MIN_RATING).fill(false)
            : new Array(this.kategorie - MIN_RATING)
                  .fill(true)
                  .concat(new Array(MAX_RATING - this.kategorie).fill(false));

    geburtsdatum: Date;

    interessen: Array<InteressenType>;

    // wird aufgerufen von fromServer() oder von fromForm()
    // eslint-disable-next-line max-params
    private constructor(
        public _id: string | undefined,
        public nachname: string,
        public kategorie: number,
        public geschlecht: GeschlechtType,
        interessen: Array<InteressenType>,
        geburtsdatum: Date,
        public umsatz: Umsatz,
        public newsletter: boolean | undefined,
        public username: string,
        public version: number | undefined,
        public user: User,
        public email: string,
        public adresse: Adresse,
        public homepage: string,
        public familienstand: FamilienstandType,
    ) {
        // TODO Parsing, ob der Geburtsdatum-String valide ist
        this.geburtsdatum =
            geburtsdatum === undefined ? new Date() : new Date(geburtsdatum);
        console.log('Kunde(): this=', this);

        this.interessen = interessen;
    }

    /**
     * Ein Kunde-Objekt mit JSON-Daten erzeugen, die von einem RESTful Web
     * Service kommen.
     * @param kunde JSON-Objekt mit Daten vom RESTful Web Server
     * @return Das initialisierte Kunde-Objekt
     */
    static fromServer(kundeServer: KundeServer, etag?: string) {
        let selfLink: string | undefined;
        const { _links } = kundeServer;
        if (_links !== undefined) {
            const { self } = _links;
            selfLink = self.href;
        }
        let id: string | undefined;
        if (selfLink !== undefined) {
            const lastSlash = selfLink.lastIndexOf('/');
            id = selfLink.slice(lastSlash + 1);
        }

        let version: number | undefined;
        if (etag !== undefined) {
            // Anfuehrungszeichen am Anfang und am Ende entfernen
            const versionString = etag.slice(1, -1);
            version = Number.parseInt(versionString, 10);
        }

        // eslint-disable-next-line prefer-const
        let geburtsdatum = new Date(kundeServer.geburtsdatum);

        const kunde = new Kunde(
            id,
            kundeServer.nachname,
            kundeServer.kategorie,
            kundeServer.geschlecht,
            kundeServer.interessen,
            geburtsdatum,
            kundeServer.umsatz,
            kundeServer.newsletter,
            kundeServer.username,
            version,
            kundeServer.user,
            kundeServer.email,
            kundeServer.adresse,
            kundeServer.homepage,
            kundeServer.familienstand,
        );
        console.log('Kunde.fromServer(): kunde=', kunde);
        return kunde;
    }

    /**
     * Ein Kunde-Objekt mit JSON-Daten erzeugen, die von einem Formular kommen.
     * @param kunde JSON-Objekt mit Daten vom Formular
     * @return Das initialisierte Kunde-Objekt
     */
    static fromForm(kundeForm: KundeForm) {
        console.log('Kunde.fromForm(): kundeForm=', kundeForm);

        const adresse: Adresse = {
            plz: kundeForm.adresse.plz,
            ort: kundeForm.adresse.ort,
        };

        const user: User = {
            username: kundeForm.nachname.toLowerCase(),
            password: '123',
        };

        // eslint-disable-next-line prefer-const
        let sendGeburtsdatum = new Date(kundeForm.geburtsdatum);

        const kunde = new Kunde(
            kundeForm._id,
            kundeForm.nachname,
            Number(kundeForm.kategorie),
            kundeForm.geschlecht,
            kundeForm.interessen,
            sendGeburtsdatum,
            kundeForm.umsatz,
            kundeForm.newsletter,
            kundeForm.username,
            kundeForm.version,
            user,
            kundeForm.email,
            adresse,
            kundeForm.homepage,
            kundeForm.familienstand,
        );
        console.log('Kunde.fromForm(): kunde=', kunde);
        return kunde;
    }

    // Property in TypeScript wie in C#
    // https://www.typescriptlang.org/docs/handbook/classes.html#accessors
    get datumFormatted() {
        // z.B. 7. Mai 2020
        const formatter = new Intl.DateTimeFormat('de', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
        });
        return this.geburtsdatum === undefined
            ? ''
            : formatter.format(this.geburtsdatum);
    }

    /**
     * Abfrage, ob im Buchtitel der angegebene Teilstring enthalten ist. Dabei
     * wird nicht auf Gross-/Kleinschreibung geachtet.
     * @param nachname Zu &uuml;berpr&uuml;fender Teilstring
     * @return true, falls der Teilstring im Buchtitel enthalten ist. Sonst
     *         false.
     */
    containsTitel(nachname: string) {
        return this.nachname === undefined
            ? false
            : this.nachname.toLowerCase().includes(nachname.toLowerCase());
    }

    /**
     * Die Bewertung ("kategorie") des Buches um 1 erh&ouml;hen
     */
    rateUp() {
        if (this.kategorie !== undefined && this.kategorie < MAX_RATING) {
            this.kategorie++;
        }
    }

    /**
     * Die Bewertung ("kategorie") des Buches um 1 erniedrigen
     */
    rateDown() {
        if (this.kategorie !== undefined && this.kategorie > MIN_RATING) {
            this.kategorie--;
        }
    }

    /**
     * Abfrage, ob das Kunde dem angegebenen Interessen zugeordnet ist.
     * @param interessen der Name des Verlags
     * @return true, falls das Kunde dem Interessen zugeordnet ist. Sonst false.
     */
    hasVerlag(interessen: Array<InteressenType>) {
        return this.interessen === interessen;
    }

    /**
     * Aktualisierung der Stammdaten des Kunde-Objekts.
     * @param nachname Der neue Buchtitel
     * @param kategorie Die neue Bewertung
     * @param geschlecht Die neue Buchart (WEIBLICH oder MAENNLICH)
     * @param interessen Der neue Interessen
     * @param umsatz Der neue Umsatz
     * @param rabatt Der neue Rabatt
     */
    // eslint-disable-next-line max-params
    updateStammdaten(
        nachname: string,
        geschlecht: GeschlechtType,
        interessen: Array<InteressenType>,
        kategorie: number,
        geburtsdatum: Date | undefined,
        umsatz: Umsatz,
        username: string,
    ) {
        this.nachname = nachname;
        this.geschlecht = geschlecht;
        this.interessen = interessen;
        this.kategorie = kategorie;
        this.ratingArray =
            kategorie === undefined
                ? new Array(MAX_RATING - MIN_RATING).fill(false)
                : new Array(kategorie - MIN_RATING).fill(true);
        this.geburtsdatum =
            geburtsdatum === undefined ? new Date() : geburtsdatum;
        this.umsatz = umsatz;
        this.username = username;
    }

    /**
     * Konvertierung des Buchobjektes in ein JSON-Objekt f&uuml;r den RESTful
     * Web Service.
     * @return Das JSON-Objekt f&uuml;r den RESTful Web Service
     */
    toJSON(): KundeServer {
        const { geburtsdatum } = this;

        console.log(`toJson(): geburtsdatum=${geburtsdatum}`);
        return {
            _id: this._id,
            nachname: this.nachname,
            kategorie: this.kategorie,
            geschlecht: this.geschlecht,
            interessen: this.interessen,
            geburtsdatum,
            umsatz: this.umsatz,
            newsletter: this.newsletter,
            username: this.username,
            version: this.version,
            user: this.user,
            email: this.email,
            adresse: this.adresse,
            homepage: this.homepage,
            familienstand: this.familienstand,
        };
    }

    toString() {
        return JSON.stringify(this, null, Kunde.SPACE);
    }
}
/* eslint-enable max-lines */
