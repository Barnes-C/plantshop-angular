import { KundeServer } from './kunde';
export class KundenRequest {
    _embedded: {
        kundeList: Array<KundeServer>;
    };
}
