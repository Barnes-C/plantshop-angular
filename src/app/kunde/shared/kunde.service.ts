/* eslint-disable max-lines */

import { BASE_URI, BUECHER_PATH_REST } from '../../shared';
import type { GeschlechtType, InteressenType, KundeServer } from './kunde';
// eslint-disable-next-line sort-imports
import type {
    ChartColor,
    ChartConfiguration,
    ChartData,
    ChartDataSets,
} from 'chart.js';
// Bereitgestellt durch HttpClientModule
// HttpClientModule enthaelt nur Services, keine Komponenten
import {
    HttpClient,
    HttpErrorResponse,
    HttpHeaders,
    HttpParams,
} from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { Kunde } from './kunde';
import { KundenRequest } from './kundenRequest';
// eslint-disable-next-line sort-imports
import { DiagrammService } from '../../shared/diagramm.service';
import { Injectable } from '@angular/core';
// https://github.com/ReactiveX/rxjs/blob/master/src/internal/Subject.ts
// https://github.com/ReactiveX/rxjs/blob/master/src/internal/Observable.ts
import { Subject } from 'rxjs';

// Methoden der Klasse HttpClient
//  * get(url, options) – HTTP GET request
//  * post(url, body, options) – HTTP POST request
//  * put(url, body, options) – HTTP PUT request
//  * patch(url, body, options) – HTTP PATCH request
//  * delete(url, options) – HTTP DELETE request

// Eine Service-Klasse ist eine "normale" Klasse gemaess ES 2015, die mittels
// DI in eine Komponente injiziert werden kann, falls sie innerhalb von
// provider: [...] bei einem Modul oder einer Komponente bereitgestellt wird.
// Eine Komponente realisiert gemaess MVC-Pattern den Controller und die View.
// Die Anwendungslogik wird vom Controller an Service-Klassen delegiert.

/**
 * Die Service-Klasse zu B&uuml;cher wird zum "Root Application Injector"
 * hinzugefuegt und ist in allen Klassen der Webanwendung verfuegbar.
 */
/* eslint-disable no-underscore-dangle */
@Injectable({ providedIn: 'root' })
export class BuchService {
    // Observables = Event-Streaming mit Promises
    // Subject statt Basisklasse Observable:
    // in find() und findById() wird next() aufgerufen
    readonly buecherSubject = new Subject<Array<Kunde>>();

    readonly buchSubject = new Subject<Kunde>();

    readonly errorSubject = new Subject<string | number>();

    private readonly baseUriBuecher!: string;

    private _buch!: Kunde;

    /**
     * @param diagrammService injizierter DiagrammService
     * @param httpClient injizierter Service HttpClient (von Angular)
     * @return void
     */
    constructor(
        private readonly diagrammService: DiagrammService,
        private readonly httpClient: HttpClient,
    ) {
        this.baseUriBuecher = `${BASE_URI}/${BUECHER_PATH_REST}`;
        console.log(
            `BuchService.constructor(): baseUriBuch=${this.baseUriBuecher}`,
        );
    }

    /**
     * Ein Kunde-Objekt puffern.
     * @param kunde Das Kunde-Objekt, das gepuffert wird.
     * @return void
     */
    set kunde(kunde: Kunde) {
        console.log('BuchService.set kunde()', kunde);
        this._buch = kunde;
    }

    /**
     * Kunden suchen
     * @param suchkriterien Die Suchkriterien
     */
    async find(suchkriterien: Suchkriterien | undefined) {
        console.log('BuchService.find(): suchkriterien=', suchkriterien);
        const parameters = this.suchkriterienToHttpParams(suchkriterien);
        const uri = this.baseUriBuecher;
        console.log(`BuchService.find(): uri=${uri}`);

        // Observable.subscribe() aus RxJS liefert ein Subscription Objekt,
        // mit dem man den Request abbrechen ("cancel") kann
        // https://angular.io/guide/http
        // https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/operators/subscribe.md
        // http://stackoverflow.com/questions/34533197/what-is-the-difference-between-rx-observable-subscribe-and-foreach
        // https://xgrommx.github.io/rx-book/content/observable/observable_instance_methods/subscribe.html
        let kunden;
        try {
            const kundenServer = await this.httpClient
                .get<Array<KundeServer>>(uri, { params: parameters })
                .pipe(
                    map(
                        (x: any) => x._embedded.kundeList as Array<KundeServer>,
                    ),
                )
                .toPromise();
            console.log(`Der Array hat die Länge ${kundenServer.length}`);
            kunden = kundenServer.map(kunde => Kunde.fromServer(kunde));

            // map((clients: Client[]) => clients.map(client => client.address))
        } catch (error) {
            console.log(error.message);
        }
        return kunden;

        // Same-Origin-Policy verhindert Ajax-Datenabfragen an einen Server in
        // einer anderen Domain. JSONP (= JSON mit Padding) ermoeglicht die
        // Uebertragung von JSON-Daten ueber Domaingrenzen.
        // In Angular gibt es dafuer den Service Jsonp.
    }

    /**
     * Ein Kunde anhand der ID suchen
     * @param id Die ID des gesuchten Buchs
     */
    // eslint-disable-next-line max-lines-per-function
    findById(id: string | undefined) {
        console.log(`BuchService.findById(): id=${id}`);

        // Gibt es ein gepuffertes Kunde mit der gesuchten ID und Versionsnr.?
        if (
            this._buch !== undefined &&
            this._buch._id === id &&
            this._buch.version !== undefined
        ) {
            console.log(
                `BuchService.findById(): Kunde gepuffert, version=${this._buch.version}`,
            );
            this.buchSubject.next(this._buch);
            return;
        }
        if (id === undefined) {
            console.log('BuchService.findById(): Keine Id');
            return;
        }

        // Ggf wegen fehlender Versionsnummer (im ETag) nachladen
        const uri = `${this.baseUriBuecher}/${id}`;

        const errorFn = (error: HttpErrorResponse) => {
            if (error.error instanceof ProgressEvent) {
                console.error(
                    'BuchService.findById(): errorFn(): Client- oder Netzwerkfehler',
                    error.error,
                );
                this.errorSubject.next(-1);
                return;
            }

            const { status } = error;
            console.log(
                `BuchService.findById(): errorFn(): status=${status}` +
                    'Response-Body=',
                error.error,
            );
            this.errorSubject.next(status);
        };

        console.log('BuchService.findById(): GET-Request');

        let body: KundeServer | null;
        let etag: string | undefined | null;
        return this.httpClient
            .get<KundeServer>(uri, { observe: 'response' })
            .pipe(
                filter(response => {
                    console.log(
                        'BuchService.findById(): filter(): response=',
                        response,
                    );
                    ({ body } = response);

                    return body !== undefined && body !== null;
                }),
                filter(response => {
                    etag = response.headers.get('ETag');
                    console.log(`etag = ${etag}`);

                    return etag !== undefined && etag !== null;
                }),

                map(_ => {
                    if (body !== null) {
                        if (etag === null) {
                            etag = undefined;
                        }
                        this._buch = Kunde.fromServer(body, etag);
                    }
                    return this._buch;
                }),
            )
            .subscribe(kunde => this.buchSubject.next(kunde), errorFn);
    }

    /**
     * Ein neues Kunde anlegen
     * @param neuesBuch Das JSON-Objekt mit dem neuen Kunde
     * @param successFn Die Callback-Function fuer den Erfolgsfall
     * @param errorFn Die Callback-Function fuer den Fehlerfall
     */
    save(
        kunde: Kunde,
        successFn: (location: string | undefined) => void,
        errorFn: (status: number, errors: { [s: string]: unknown }) => void,
    ) {
        console.log('BuchService.save(): kunde=', kunde);
        kunde.geburtsdatum = new Date();

        const errorFnPost = (error: HttpErrorResponse) => {
            if (error.error instanceof Error) {
                console.error(
                    'BuchService.save(): errorFnPost(): Client- oder Netzwerkfehler',
                    error.error.message,
                );
            } else if (errorFn === undefined) {
                console.error('errorFnPost', error);
            } else {
                // z.B. {nachname: ..., interessen: ..., username: ...}
                errorFn(error.status, error.error);
            }
        };

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Accept: 'text/plain',
        });
        return this.httpClient
            .post(this.baseUriBuecher, kunde.toJSON(), {
                headers,
                observe: 'response',
                responseType: 'text',
            })
            .pipe(
                map(response => {
                    console.log(
                        'BuchService.save(): map(): response',
                        response,
                    );
                    const headersResponse = response.headers;
                    const location = headersResponse.get('Location');
                    const locationString =
                        location === null ? undefined : location;
                    return locationString;
                }),
            )
            .subscribe(location => successFn(location), errorFnPost);
    }

    /**
     * Ein vorhandenes Kunde aktualisieren
     * @param kunde Das JSON-Objekt mit den aktualisierten Buchdaten
     * @param successFn Die Callback-Function fuer den Erfolgsfall
     * @param errorFn Die Callback-Function fuer den Fehlerfall
     */
    update(
        kunde: Kunde,
        successFn: () => void,
        errorFn: (
            status: number,
            errors: { [s: string]: unknown } | undefined,
        ) => void,
    ) {
        console.log('BuchService.update(): kunde=', kunde);

        const { version } = kunde;
        if (version === undefined) {
            console.error(`Keine Versionsnummer fuer das Kunde ${kunde._id}`);
            return;
        }
        const successFnPut = () => {
            successFn();
            // Wenn Update erfolgreich war, dann wurde serverseitig die Versionsnr erhoeht
            if (kunde.version === undefined) {
                kunde.version = 1;
            } else {
                kunde.version++;
            }
        };
        const errorFnPut = (error: HttpErrorResponse) => {
            if (error.error instanceof Error) {
                console.error(
                    'Client-seitiger oder Netzwerkfehler',
                    error.error.message,
                );
            } else if (errorFn === undefined) {
                console.error('errorFnPut', error);
            } else {
                errorFn(error.status, error.error);
            }
        };

        const uri = `${this.baseUriBuecher}/${kunde._id}`;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Accept: 'text/plain',
            'If-Match': `"${version}"`,
        });
        console.log('headers=', headers);
        return this.httpClient
            .put(uri, kunde, { headers })
            .subscribe(successFnPut, errorFnPut);
    }

    /**
     * Ein Kunde l&ouml;schen
     * @param kunde Das JSON-Objekt mit dem zu loeschenden Kunde
     * @param successFn Die Callback-Function fuer den Erfolgsfall
     * @param errorFn Die Callback-Function fuer den Fehlerfall
     */
    remove(
        kunde: Kunde,
        successFn: (() => void) | undefined,
        errorFn: (status: number) => void,
    ) {
        console.log('BuchService.remove(): kunde=', kunde);
        const uri = `${this.baseUriBuecher}/${kunde._id}`;

        const errorFnDelete = (error: HttpErrorResponse) => {
            if (error.error instanceof Error) {
                console.error(
                    'Client-seitiger oder Netzwerkfehler',
                    error.error.message,
                );
            } else if (errorFn === undefined) {
                console.error('errorFnPut', error);
            } else {
                errorFn(error.status);
            }
        };

        return this.httpClient.delete(uri).subscribe(successFn, errorFnDelete);
    }

    // http://www.sitepoint.com/15-best-javascript-charting-libraries
    // http://thenextweb.com/dd/2015/06/12/20-best-javascript-chart-libraries
    // http://mikemcdearmon.com/portfolio/techposts/charting-libraries-using-d3

    // D3 (= Data Driven Documents) https://d3js.org ist das fuehrende Produkt
    // fuer Datenvisualisierung:
    //  initiale Version durch die Dissertation von Mike Bostock
    //  gesponsort von der New York Times, seinem heutigen Arbeitgeber
    //  basiert auf SVG = scalable vector graphics: Punkte, Linien, Kurven, ...
    //  ca 250.000 Downloads/Monat bei https://www.npmjs.com
    //  https://github.com/mbostock/d3 mit ueber 100 Contributors

    // Weitere Alternativen:
    // Google Charts: https://google-developers.appspot.com/chart
    // Chartist.js:   http://gionkunz.github.io/chartist-js
    // n3-chart:      http://n3-charts.github.io/line-chart

    // Chart.js ist deutlich einfacher zu benutzen als D3
    //  basiert auf <canvas>
    //  ca 25.000 Downloads/Monat bei https://www.npmjs.com
    //  https://github.com/nnnick/Chart.js mit ueber 60 Contributors

    /**
     * Ein Balkendiagramm erzeugen und bei einem Tag <code>canvas</code>
     * einf&uuml;gen.
     * @param chartElement Das HTML-Element zum Tag <code>canvas</code>
     */
    createBarChart(chartElement: HTMLCanvasElement) {
        console.log('BuchService.createBarChart()');
        const uri = this.baseUriBuecher;
        return this.httpClient
            .get<Array<KundeServer>>(uri)
            .pipe(
                // ID aus Self-Link
                map(kunden => kunden.map(kunde => this.setBuchId(kunde))),
                map(kunden => {
                    const buecherGueltig = kunden.filter(
                        b => b._id !== undefined && b.kategorie !== undefined,
                    );
                    const labels = buecherGueltig
                        .map(b => b._id)
                        .map(id => {
                            if (id === undefined) {
                                return '?';
                            }
                            return id;
                        });
                    console.log(
                        'BuchService.createBarChart(): labels:',
                        labels,
                    );

                    const data = buecherGueltig.map(b => b.kategorie);
                    /* eslint-disable array-bracket-newline */
                    const datasets: Array<ChartDataSets> = [
                        { label: 'Bewertung', data },
                    ];
                    /* eslint-enable array-bracket-newline */

                    const config: ChartConfiguration = {
                        type: 'bar',
                        data: { labels, datasets },
                    };
                    return config;
                }),
            )
            .subscribe(config =>
                this.diagrammService.createChart(chartElement, config),
            );
    }

    /**
     * Ein Liniendiagramm erzeugen und bei einem Tag <code>canvas</code>
     * einf&uuml;gen.
     * @param chartElement Das HTML-Element zum Tag <code>canvas</code>
     */
    createLinearChart(chartElement: HTMLCanvasElement) {
        console.log('BuchService.createLinearChart()');
        const uri = this.baseUriBuecher;

        return this.httpClient
            .get<Array<KundeServer>>(uri)
            .pipe(
                // ID aus Self-Link
                map(kunden => kunden.map(b => this.setBuchId(b))),
                map(kunden => {
                    const buecherGueltig = kunden.filter(
                        b => b._id !== undefined && b.kategorie !== undefined,
                    );
                    const labels = buecherGueltig
                        .map(b => b._id)
                        .map(id => {
                            if (id === undefined) {
                                return '?';
                            }
                            return id;
                        });
                    console.log(
                        'BuchService.createLinearChart(): labels:',
                        labels,
                    );

                    const data = buecherGueltig.map(b => b.kategorie);
                    /* eslint-disable array-bracket-newline */
                    const datasets: Array<ChartDataSets> = [
                        { label: 'Bewertung', data },
                    ];
                    /* eslint-enable array-bracket-newline */

                    const config: ChartConfiguration = {
                        type: 'line',
                        data: { labels, datasets },
                    };
                    return config;
                }),
            )
            .subscribe(config =>
                this.diagrammService.createChart(chartElement, config),
            );
    }

    /**
     * Ein Tortendiagramm erzeugen und bei einem Tag <code>canvas</code>
     * einf&uuml;gen.
     * @param chartElement Das HTML-Element zum Tag <code>canvas</code>
     */
    // eslint-disable-next-line max-lines-per-function
    createPieChart(chartElement: HTMLCanvasElement) {
        console.log('BuchService.createPieChart()');
        const uri = this.baseUriBuecher;

        return this.httpClient
            .get<KundenRequest>(uri)
            .pipe(
                // ID aus Self-Link
                map(kundenRequest =>
                    kundenRequest._embedded.kundeList.map(kunde =>
                        this.setBuchId(kunde),
                    ),
                ),
                map(kunden => {
                    const buecherGueltig = kunden.filter(
                        b => b._id !== undefined && b.kategorie !== undefined,
                    );
                    const labels = buecherGueltig
                        .map(b => b._id)
                        .map(id => {
                            if (id === undefined) {
                                return '?';
                            }
                            return id;
                        });
                    console.log(
                        'BuchService.createPieChart(): labels:',
                        labels,
                    );
                    const ratings = buecherGueltig.map(b => b.kategorie);

                    const backgroundColor: Array<ChartColor> = [];
                    const hoverBackgroundColor: Array<ChartColor> = [];
                    for (let i = 0; i < ratings.length; i++) {
                        backgroundColor.push(
                            this.diagrammService.getBackgroundColor(i),
                        );
                        hoverBackgroundColor.push(
                            this.diagrammService.getHoverBackgroundColor(i),
                        );
                    }

                    const data: ChartData = {
                        labels,
                        datasets: [
                            {
                                data: ratings,
                                backgroundColor,
                                hoverBackgroundColor,
                            },
                        ],
                    };

                    const config: ChartConfiguration = { type: 'pie', data };
                    return config;
                }),
            )
            .subscribe(config =>
                this.diagrammService.createChart(chartElement, config),
            );
    }

    /**
     * Suchkriterien in Request-Parameter konvertieren.
     * @param suchkriterien Suchkriterien fuer den GET-Request.
     * @return Parameter fuer den GET-Request
     */
    private suchkriterienToHttpParams(
        suchkriterien: Suchkriterien | undefined,
    ): HttpParams {
        console.log(
            'BuchService.suchkriterienToHttpParams(): suchkriterien=',
            suchkriterien,
        );
        let httpParameters = new HttpParams();

        if (suchkriterien === undefined) {
            return httpParameters;
        }

        const { nachname, email, interessen, geschlecht } = suchkriterien;

        if (nachname !== '') {
            httpParameters = httpParameters.set('nachname', nachname);
        }
        if (email !== '') {
            httpParameters = httpParameters.set('username', email);
        }
        if (geschlecht !== '') {
            httpParameters = httpParameters.set('geschlecht', geschlecht);
        }
        if (interessen !== '') {
            httpParameters = httpParameters.set('interessen', interessen);
        }
        return httpParameters;
    }

    private setBuchId(kunde: KundeServer) {
        const { _links } = kunde;
        if (_links !== undefined) {
            const selfLink = _links.self.href;
            if (typeof selfLink === 'string') {
                const lastSlash = selfLink.lastIndexOf('/');
                kunde._id = selfLink.slice(lastSlash + 1);
            }
        }
        if (kunde._id === undefined) {
            kunde._id = 'undefined';
        }
        return kunde;
    }
}
/* eslint-enable no-underscore-dangle */

export interface Suchkriterien {
    nachname: string;
    email: string;
    interessen: InteressenType | '';
    geschlecht: GeschlechtType | '';
}
