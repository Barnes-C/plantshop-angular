import { inject, TestBed } from '@angular/core/testing';
import {
    HttpClientTestingModule,
    HttpTestingController,
} from '@angular/common/http/testing';
import { BuchService } from './kunde.service';
import { BASE_URI } from '../../shared';
// import { mockPflanze, mockPflanzen } from './mockData';

const idVorhanden = '00000000-0000-0000-0000-000000000000';

// -----------------------------------------------------------------------------
// T e s t s
// -----------------------------------------------------------------------------
describe('BuchService', () => {
    let service: BuchService;
    let httpPflanze: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [BuchService],
        });
        service = TestBed.inject(BuchService);
        httpPflanze = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        // Every time we test our data
        httpPflanze.verify();
    });

    it('should be created', inject([BuchService], () => {
        // eslint-disable-next-line no-unused-expressions
        expect(service).toBeTruthy;
    }));

    // it('should get a specific Pflanze by ID', () => {
    //     // Act
    //     service.findById(idVorhanden).subscribe(p => {
    //         expect(p).toEqual();
    //     });

    //     // Assert
    //     const request = httpPflanze.expectOne(BASE_URI);
    //     expect(request.request.method).toBe('GET');
    // });

    // it('should get all Pflanzen via GET', () => {
    //     // Act
    //     service.find().subscribe(result => {
    //         expect(result).toEqual(mockPflanzen);
    //     });

    //     // Assert
    //     const request = httpPflanze.expectOne(BASE_URI);
    //     expect(request.request.method).toBe('GET');
    // });

    // it('should get a specific Pflanze by Suchkriterium', () => {
    //     // Act
    //     service.find();

    //     // Assert
    //     const request = httpPflanze.expectOne(BASE_URI);
    //     expect(request.request.method).toBe('GET');
    // });

    // it('should get a specific Pflanze by ID', () => {
    //     // Act
    //     service.findById(idVorhanden).subscribe(p => {
    //         expect(p).toEqual();
    //     });

    //     // Assert
    //     const request = httpPflanze.expectOne(BASE_URI);
    //     expect(request.request.method).toBe('GET');
    // });
});
