// Bereitgestellt durch das RouterModule
import { ActivatedRoute, Router } from '@angular/router';
import { BuchService, Kunde } from '../../shared';
import { Component, Input } from '@angular/core';
import { HttpStatus, easeIn, easeOut } from '../../../shared';
import type {
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChanges,
} from '@angular/core';
import { AuthService } from '../../../auth/auth.service';
import { NgLocalization } from '@angular/common';
import { Subscription } from 'rxjs';
import type { Suchkriterien } from '../../shared/types';

/**
 * Komponente f&uuml;r das Tag <code>hs-suchergebnis</code>, um zun&auml;chst
 * das Warten und danach das Ergebnis der Suche anzuzeigen, d.h. die gefundenen
 * B&uuml;cher oder eine Fehlermeldung.
 */
@Component({
    selector: 'hs-suchergebnis',
    templateUrl: './suchergebnis.component.html',
    animations: [easeIn, easeOut],
})
export class SuchergebnisComponent implements OnChanges, OnInit, OnDestroy {
    // Im ganzen Beispiel: lokale Speicherung des Zustands und nicht durch z.B.
    // eine Flux-Bibliothek, wie z.B. Redux http://redux.js.org

    // Property Binding: <hs-suchergebnis [waiting]="...">
    // Decorator fuer ein Attribut. Siehe InputMetadata
    @Input()
    suchkriterien: Suchkriterien | undefined;

    waiting = false;

    kunden: Array<Kunde> = [];

    neueKunden: any;

    errorMsg: string | undefined;

    isAdmin!: boolean;

    private buecherSubscription!: Subscription;

    private errorSubscription!: Subscription;

    private removeDescription: Subscription | undefined;

    // Empfehlung: Konstruktor nur fuer DI
    // eslint-disable-next-line max-params
    constructor(
        private readonly buchService: BuchService,
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly authService: AuthService,
    ) {
        console.log('SuchergebnisComponent.constructor()');
    }

    /*
    ngOnChanges(changes: SimpleChanges) {
        if (changes.suchkriterien.currentValue === undefined) {
            return;
        }

        this.waiting = true;
        this.buchService.find(this.suchkriterien);
*/
    async ngOnChanges(changes: SimpleChanges) {
        if (changes.suchkriterien.currentValue === undefined) {
            return;
        }

        this.waiting = true;

        try {
            this.neueKunden = await this.buchService.find(this.suchkriterien);
            this.kunden = this.neueKunden;
        } catch (error) {
            console.log('error');
            return;
        } finally {
            this.waiting = false;
        }

        console.log(
            'SuchergebnisComponent.ngOnChanges(): buecher=',
            this.kunden,
        );
    }

    // Attribute mit @Input() sind undefined im Konstruktor.
    // Methode zum "LifeCycle Hook" OnInit: wird direkt nach dem Konstruktor
    // aufgerufen. Entspricht @PostConstruct bei Java.
    // Weitere Methoden zum Lifecycle: ngAfterViewInit(), ngAfterContentInit()
    // https://angular.io/docs/ts/latest/guide/cheatsheet.html
    // Die Ableitung vom Interface OnInit ist nicht notwendig, aber erleichtet
    // IntelliSense bei der Verwendung von TypeScript.
    ngOnInit() {
        this.buecherSubscription = this.subscribeBuecher();
        this.errorSubscription = this.subscribeError();
        this.isAdmin = this.authService.isAdmin;
    }

    ngOnDestroy() {
        this.buecherSubscription.unsubscribe();
        this.errorSubscription.unsubscribe();

        if (this.removeDescription !== undefined) {
            this.removeDescription.unsubscribe();
        }
    }

    /**
     * Das ausgew&auml;hlte bzw. angeklickte Kunde in der Detailsseite anzeigen.
     * @param kunde Das ausgew&auml;hlte Kunde
     */
    onClick(kunde: Kunde) {
        console.log('SuchergebnisComponent.onSelect(): kunde=', kunde);
        // Puffern im Singleton, um nicht erneut zu suchen
        this.buchService.kunde = kunde;
        // TODO: NavigationExtras beim Routing
        // https://github.com/angular/angular/pull/27198
        // https://github.com/angular/angular/commit/67f4a5d4bd3e8e6a35d85500d630d94db061900b

        /* eslint-disable object-curly-newline */
        return this.router.navigate(['..', kunde._id], {
            relativeTo: this.route,
        });
    }

    /**
     * Das ausgew&auml;hlte bzw. angeklickte Kunde l&ouml;schen.
     * @param kunde Das ausgew&auml;hlte Kunde
     */
    onRemove(kunde: Kunde) {
        console.log('SuchergebnisComponent.onRemove(): kunde=', kunde);
        const successFn: (() => void) | undefined = undefined;
        const errorFn = (status: number) =>
            console.error(`Fehler beim Loeschen: status=${status}`);
        this.removeDescription = this.buchService.remove(
            kunde,
            successFn,
            errorFn,
        );
        if (this.kunden.length > 0) {
            this.kunden = this.kunden.filter((b: Kunde) => b._id !== kunde._id);
        }
    }

    /**
     * Methode, um den injizierten <code>BuchService</code> zu beobachten,
     * ob es gefundene bzw. darzustellende B&uuml;cher gibt, die in der
     * Kindkomponente f&uuml;r das Tag <code>gefundene-kunden</code>
     * dargestellt werden. Diese private Methode wird in der Methode
     * <code>ngOnInit</code> aufgerufen.
     */
    private subscribeBuecher() {
        const next = (kunden: Array<Kunde>) => {
            this.reset();
            this.errorMsg = undefined;

            this.kunden = kunden;
            console.log(
                'SuchErgebnisComponent.subscribeBuecher: this.kunden=',
                this.kunden,
            );
        };

        // Observable.subscribe() aus RxJS liefert ein Subscription Objekt,
        // mit dem man den Request auch abbrechen ("cancel") kann
        // https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/operators/subscribe.md
        // http://stackoverflow.com/questions/34533197/what-is-the-difference-between-rx-observable-subscribe-and-foreach
        // https://xgrommx.github.io/rx-book/content/observable/observable_instance_methods/subscribe.html
        // Funktion als Funktionsargument, d.h. Code als Daten uebergeben
        return this.buchService.buecherSubject.subscribe(next);
    }

    /**
     * Methode, um den injizierten <code>BuchService</code> zu beobachten,
     * ob es bei der Suche Fehler gibt, die in der Kindkomponente f&uuml;r das
     * Tag <code>error-message</code> dargestellt werden. Diese private Methode
     * wird in der Methode <code>ngOnInit</code> aufgerufen.
     */
    private subscribeError() {
        const next = (error: string | number | undefined) => {
            this.reset();
            this.kunden = [];

            console.log('SuchErgebnisComponent.subscribeError: err=', error);
            if (error === undefined) {
                this.errorMsg = 'Ein Fehler ist aufgetreten.';
                return;
            }

            if (typeof error === 'string') {
                this.errorMsg = error;
                return;
            }

            switch (error) {
                case HttpStatus.NOT_FOUND:
                    this.errorMsg = 'Keine Bücher gefunden.';
                    break;
                case HttpStatus.TOO_MANY_REQUESTS:
                    this.errorMsg =
                        'Zu viele Anfragen. Bitte versuchen Sie es später noch einmal.';
                    break;
                default:
                    this.errorMsg = 'Ein Fehler ist aufgetreten.';
                    break;
            }
            console.log(`SuchErgebnisComponent.errorMsg: ${this.errorMsg}`);
        };

        return this.buchService.errorSubject.subscribe(next);
    }

    private reset() {
        this.suchkriterien = {
            nachname: '',
            email: '',
            interessen: '',
            geschlecht: '',
        };
        this.waiting = false;
    }
}

export class AnzahlLocalization extends NgLocalization {
    getPluralCategory(count: number) {
        return count === 1 ? 'single' : 'multi';
    }
}
