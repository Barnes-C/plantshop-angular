import { NgModule } from '@angular/core';
import { SucheBuecherComponent } from './suche-kunden.component';
import { SuchergebnisModule } from './suchergebnis/suchergebnis.module';
import { SuchformularModule } from './suchformular/suchformular.module';
import { Title } from '@angular/platform-browser';

// Ein Modul enthaelt logisch zusammengehoerige Funktionalitaet.
// Exportierte Komponenten koennen bei einem importierenden Modul in dessen
// Komponenten innerhalb deren Templates (= HTML-Fragmente) genutzt werden.
// SucheBuecherModule ist ein "FeatureModule", das Features fuer Kunden bereitstellt
@NgModule({
    declarations: [SucheBuecherComponent],
    exports: [SucheBuecherComponent],
    imports: [SuchergebnisModule, SuchformularModule],
    providers: [Title],
})
export class SucheBuecherModule {}
