import { Component } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-suche-nachname</code>
 */
@Component({
    selector: 'hs-suche-nachname',
    templateUrl: './suche-nachname.component.html',
})
export class SucheTitelComponent {
    nachname = '';

    constructor() {
        console.log('SucheTitelComponent.constructor()');
    }
}
