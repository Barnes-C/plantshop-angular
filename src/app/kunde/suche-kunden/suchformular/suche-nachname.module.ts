import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SucheTitelComponent } from './suche-nachname.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
    declarations: [SucheTitelComponent],
    exports: [SucheTitelComponent],
    imports: [FormsModule, MaterialModule],
})
export class SucheTitelModule {}
