import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SucheEmailComponent } from './suche-email.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
    declarations: [SucheEmailComponent],
    exports: [SucheEmailComponent],
    imports: [FormsModule, MaterialModule],
})
export class SucheEmailModule {}
