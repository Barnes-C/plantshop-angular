import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SucheSchlagwoerterComponent } from './suche-schlagwoerter.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
    declarations: [SucheSchlagwoerterComponent],
    exports: [SucheSchlagwoerterComponent],
    imports: [FormsModule, MaterialModule],
})
export class SucheSchlagwoerterModule {}
