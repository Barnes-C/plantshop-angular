import { Component } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-suche-email</code>
 */
@Component({
    selector: 'hs-suche-email',
    templateUrl: './suche-email.component.html',
})
export class SucheEmailComponent {
    email = '';

    constructor() {
        console.log('SucheEmailComponent.constructor()');
    }
}
