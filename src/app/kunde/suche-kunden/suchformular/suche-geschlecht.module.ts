import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SucheArtComponent } from './suche-geschlecht.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
    declarations: [SucheArtComponent],
    exports: [SucheArtComponent],
    imports: [FormsModule, MaterialModule],
})
export class SucheArtModule {}
