import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SucheVerlagComponent } from './suche-interessen.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
    declarations: [SucheVerlagComponent],
    exports: [SucheVerlagComponent],
    imports: [FormsModule, MaterialModule],
})
export class SucheVerlagModule {}
