import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SucheArtModule } from './suche-geschlecht.module';
import { SucheSchlagwoerterModule } from './suche-schlagwoerter.module';
import { SucheTitelModule } from './suche-nachname.module';
import { SucheEmailModule } from './suche-email.module';
import { SucheVerlagModule } from './suche-interessen.module';
import { SuchformularComponent } from './suchformular.component';

// Ein Modul enthaelt logisch zusammengehoerige Funktionalitaet.
// Exportierte Komponenten koennen bei einem importierenden Modul in dessen
// Komponenten innerhalb deren Templates (= HTML-Fragmente) genutzt werden.
// SucheBuecherModule ist ein "FeatureModule", das Features fuer Kunden bereitstellt
@NgModule({
    declarations: [SuchformularComponent],
    exports: [SuchformularComponent],
    imports: [
        HttpClientModule,
        FormsModule,
        SucheArtModule,
        SucheSchlagwoerterModule,
        SucheTitelModule,
        SucheEmailModule,
        SucheVerlagModule,
    ],
})
export class SuchformularModule {}
