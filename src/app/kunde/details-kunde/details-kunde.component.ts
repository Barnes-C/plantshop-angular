import { ActivatedRoute, Params } from '@angular/router';
import { AuthService, ROLLE_ADMIN } from '../../auth/auth.service';
import { BuchService, Kunde } from '../shared';
import type { OnDestroy, OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { HttpStatus } from '../../shared';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-kunde</code>
 */
@Component({
    selector: 'hs-details-kunde',
    templateUrl: './details-kunde.component.html',
})
export class DetailsBuchComponent implements OnInit, OnDestroy {
    waiting = true;

    kunde: Kunde | undefined;

    errorMsg: string | undefined;

    isAdmin!: boolean;

    private buchSubscription!: Subscription;

    private errorSubscription!: Subscription;

    private idParamSubscription!: Subscription;

    private isAdminSubscription!: Subscription;

    private findByIdSubscription: Subscription | undefined;

    // eslint-disable-next-line max-params
    constructor(
        private readonly buchService: BuchService,
        private readonly titleService: Title,
        private readonly route: ActivatedRoute,
        private readonly authService: AuthService,
    ) {
        console.log('DetailsBuchComponent.constructor()');
    }

    ngOnInit() {
        // Die Beobachtung starten, ob es ein zu darzustellendes Kunde oder
        // einen Fehler gibt.
        this.buchSubscription = this.subscribeBuch();
        this.errorSubscription = this.subscribeError();
        this.idParamSubscription = this.subscribeIdParam();

        // Initialisierung, falls zwischenzeitlich der Browser geschlossen wurde
        this.isAdmin = this.authService.isAdmin;
        this.isAdminSubscription = this.subscribeIsAdmin();
    }

    ngOnDestroy() {
        this.buchSubscription.unsubscribe();
        this.errorSubscription.unsubscribe();
        this.idParamSubscription.unsubscribe();
        this.isAdminSubscription.unsubscribe();

        if (this.findByIdSubscription !== undefined) {
            this.findByIdSubscription.unsubscribe();
        }
    }

    private subscribeBuch() {
        const next = (kunde: Kunde) => {
            this.waiting = false;
            this.kunde = kunde;
            console.log('DetailsBuchComponent.kunde=', this.kunde);
            const nachname =
                this.kunde === undefined
                    ? 'Details'
                    : `Details ${this.kunde._id}`;
            this.titleService.setTitle(nachname);
        };
        return this.buchService.buchSubject.subscribe(next);
    }

    private subscribeError() {
        const next = (error: string | number | undefined) => {
            this.waiting = false;
            if (error === undefined) {
                this.errorMsg = 'Ein Fehler ist aufgetreten.';
                return;
            }

            if (typeof error === 'string') {
                this.errorMsg = error;
                return;
            }

            this.errorMsg =
                error === HttpStatus.NOT_FOUND
                    ? 'Kein Kunde gefunden.'
                    : 'Ein Fehler ist aufgetreten.';
            console.log(`DetailsBuchComponent.errorMsg: ${this.errorMsg}`);

            this.titleService.setTitle('Fehler');
        };

        return this.buchService.errorSubject.subscribe(next);
    }

    private subscribeIdParam() {
        // Pfad-Parameter aus /kunden/:id
        // UUID (oder Mongo-ID) ist ein String
        const next = (parameters: Params) => {
            console.log(
                'DetailsBuchComponent.subscribeIdParam(): params=',
                parameters,
            );
            this.findByIdSubscription = this.buchService.findById(
                parameters.id,
            );
        };
        // ActivatedRoute.params ist ein Observable
        return this.route.params.subscribe(next);
    }

    private subscribeIsAdmin() {
        const nextIsAdmin = (event: Array<string>) => {
            this.isAdmin = event.includes(ROLLE_ADMIN);
            console.log(
                `DetailsBuchComponent.subscribeIsAdmin(): isAdmin=${this.isAdmin}`,
            );
        };
        return this.authService.rollenSubject.subscribe(nextIsAdmin);
    }
}
