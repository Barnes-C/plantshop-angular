import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-newsletter</code>
 */
@Component({
    selector: 'hs-details-newsletter',
    templateUrl: './details-newsletter.component.html',
})
export class DetailsLieferbarComponent implements OnInit {
    @Input()
    readonly newsletter: boolean | undefined;

    ngOnInit() {
        console.log(`DetailsLieferbarComponent.newsletter=${this.newsletter}`);
    }
}
