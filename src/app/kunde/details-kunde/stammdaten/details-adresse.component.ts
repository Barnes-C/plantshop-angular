import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-adresse</code>
 */
@Component({
    selector: 'hs-details-adresse',
    templateUrl: './details-adresse.component.html',
})
export class DetailsAdresseComponent implements OnInit {
    @Input()
    readonly ort!: string | '';

    ngOnInit() {
        console.log(`DetailsAdresseComponent.adresse=${this.ort}`);
    }
}
