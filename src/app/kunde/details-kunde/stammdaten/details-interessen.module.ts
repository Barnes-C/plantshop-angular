import { CommonModule } from '@angular/common';
import { DetailsVerlagComponent } from './details-interessen.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsVerlagComponent],
    exports: [DetailsVerlagComponent],
    imports: [CommonModule],
})
export class DetailsVerlagModule {}
