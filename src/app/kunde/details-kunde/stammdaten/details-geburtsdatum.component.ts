import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-geburtsdatum</code>
 */
@Component({
    selector: 'hs-details-geburtsdatum',
    templateUrl: './details-geburtsdatum.component.html',
})
export class DetailsDatumComponent implements OnInit {
    @Input()
    readonly formatted!: string;

    ngOnInit() {
        console.log(`DetailsDatumComponent: formatted=${this.formatted}`);
    }
}
