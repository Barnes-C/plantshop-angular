import { CommonModule } from '@angular/common';
import { DetailsArtComponent } from './details-geschlecht.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsArtComponent],
    exports: [DetailsArtComponent],
    imports: [CommonModule],
})
export class DetailsArtModule {}
