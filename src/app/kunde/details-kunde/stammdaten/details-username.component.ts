import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-username</code>
 */
@Component({
    selector: 'hs-details-username',
    templateUrl: './details-username.component.html',
})
export class DetailsIsbnComponent implements OnInit {
    @Input()
    readonly username!: string;

    ngOnInit() {
        console.log(`DetailsIsbnComponent.username=${this.username}`);
    }
}
