import { DetailsIsbnComponent } from './details-username.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsIsbnComponent],
    exports: [DetailsIsbnComponent],
})
export class DetailsIsbnModule {}
