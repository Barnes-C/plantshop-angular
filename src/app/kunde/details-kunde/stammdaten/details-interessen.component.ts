import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';
// eslint-disable-next-line sort-imports
import type { InteressenType } from '../../shared/kunde';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-interessen</code>
 */
@Component({
    selector: 'hs-details-interessen',
    templateUrl: './details-interessen.component.html',
})
export class DetailsVerlagComponent implements OnInit {
    @Input()
    readonly interessen: InteressenType | undefined | '';

    ngOnInit() {
        console.log(`DetailsVerlagComponent.interessen=${this.interessen}`);
    }
}
