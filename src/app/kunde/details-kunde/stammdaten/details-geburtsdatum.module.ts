import { DetailsDatumComponent } from './details-geburtsdatum.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsDatumComponent],
    exports: [DetailsDatumComponent],
})
export class DetailsDatumModule {}
