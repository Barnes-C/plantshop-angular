import { Component, Input } from '@angular/core';
import type { GeschlechtType } from '../../shared/kunde';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-geschlecht</code>
 */
@Component({
    selector: 'hs-details-geschlecht',
    templateUrl: './details-geschlecht.component.html',
})
export class DetailsArtComponent implements OnInit {
    @Input()
    readonly geschlecht!: GeschlechtType;

    ngOnInit() {
        console.log(`DetailsArtComponent.geschlecht=${this.geschlecht}`);
    }
}
