import { CommonModule } from '@angular/common';
import { DetailsAdresseModule } from './details-adresse.module';
import { DetailsArtModule } from './details-geschlecht.module';
import { DetailsDatumModule } from './details-geburtsdatum.module';
import { DetailsEmailModule } from './details-email.module';
import { DetailsIsbnModule } from './details-username.module';
import { DetailsLieferbarModule } from './details-newsletter.module';
import { DetailsRabattModule } from './details-rabatt.module';
import { DetailsStammdatenComponent } from './details-stammdaten.component';
import { DetailsTitelModule } from './details-nachname.module';
import { DetailsVerlagModule } from './details-interessen.module';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsStammdatenComponent],
    exports: [DetailsStammdatenComponent],
    imports: [
        CommonModule,
        DetailsAdresseModule,
        DetailsArtModule,
        DetailsDatumModule,
        DetailsEmailModule,
        DetailsIsbnModule,
        DetailsLieferbarModule,
        DetailsRabattModule,
        DetailsTitelModule,
        DetailsVerlagModule,
    ],
})
export class DetailsStammdatenModule {}
