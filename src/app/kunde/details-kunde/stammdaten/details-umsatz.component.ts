import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-umsatz</code>
 */
@Component({
    selector: 'hs-details-umsatz',
    templateUrl: './details-umsatz.component.html',
})
export class DetailsPreisComponent implements OnInit {
    @Input()
    readonly umsatz!: number | '';

    ngOnInit() {
        console.log(`DetailsPreisComponent.umsatz=${this.umsatz}`);
    }
}
