import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-rabatt</code>
 */
@Component({
    selector: 'hs-details-rabatt',
    templateUrl: './details-rabatt.component.html',
})
export class DetailsRabattComponent implements OnInit {
    @Input()
    readonly rabatt!: number | '';

    ngOnInit() {
        console.log(`DetailsRabattComponent.rabatt=${this.rabatt}`);
    }
}
