import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-nachname</code>
 */
@Component({
    selector: 'hs-details-nachname',
    templateUrl: './details-nachname.component.html',
})
export class DetailsTitelComponent implements OnInit {
    @Input()
    readonly nachname!: string;

    ngOnInit() {
        console.log(`DetailsTitelComponent.nachname=${this.nachname}`);
    }
}
