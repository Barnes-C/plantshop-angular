import { CommonModule } from '@angular/common';
import { DetailsLieferbarComponent } from './details-newsletter.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsLieferbarComponent],
    exports: [DetailsLieferbarComponent],
    imports: [CommonModule],
})
export class DetailsLieferbarModule {}
