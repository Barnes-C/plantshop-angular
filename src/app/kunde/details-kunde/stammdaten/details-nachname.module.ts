import { CommonModule } from '@angular/common';
import { DetailsTitelComponent } from './details-nachname.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsTitelComponent],
    exports: [DetailsTitelComponent],
    imports: [CommonModule],
})
export class DetailsTitelModule {}
