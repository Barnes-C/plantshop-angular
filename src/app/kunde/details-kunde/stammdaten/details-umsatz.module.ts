import { CommonModule } from '@angular/common';
import { DetailsPreisComponent } from './details-umsatz.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsPreisComponent],
    exports: [DetailsPreisComponent],
    imports: [CommonModule],
})
export class DetailsPreisModule {}
