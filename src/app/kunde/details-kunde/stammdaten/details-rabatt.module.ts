import { CommonModule } from '@angular/common';
import { DetailsRabattComponent } from './details-rabatt.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsRabattComponent],
    exports: [DetailsRabattComponent],
    imports: [CommonModule],
})
export class DetailsRabattModule {}
