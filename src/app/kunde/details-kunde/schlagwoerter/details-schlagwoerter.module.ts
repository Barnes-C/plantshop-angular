import { CommonModule } from '@angular/common';
import { DetailsSchlagwoerterComponent } from './details-schlagwoerter.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [DetailsSchlagwoerterComponent],
    exports: [DetailsSchlagwoerterComponent],
    imports: [CommonModule],
})
export class DetailsSchlagwoerterModule {}
