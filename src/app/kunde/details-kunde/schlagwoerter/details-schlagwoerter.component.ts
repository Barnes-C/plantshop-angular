import { Component, Input } from '@angular/core';
import type { OnInit } from '@angular/core';

/**
 * Komponente f&uuml;r das Tag <code>hs-details-schlagwoerter</code>
 */
@Component({
    selector: 'hs-details-schlagwoerter',
    templateUrl: './details-schlagwoerter.component.html',
})
export class DetailsSchlagwoerterComponent implements OnInit {
    // <hs-schlagwoerter [values]="kunde.schlagwoerter">
    // Decorator fuer ein Attribut. Siehe InputMetadata
    @Input()
    readonly values!: Array<string>;

    ngOnInit() {
        console.log('DetailsSchlagwoerterComponent.values=', this.values);
    }
}
