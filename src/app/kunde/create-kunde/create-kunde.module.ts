import { CommonModule } from '@angular/common';
import { CreateArtModule } from './create-geschlecht.module';
import { CreateBuchComponent } from './create-kunde.component';
import { CreateDatumModule } from './create-geburtsdatum.module';
import { CreateIsbnModule } from './create-username.module';
import { CreateLieferbarModule } from './create-newsletter.module';
import { CreatePreisModule } from './create-umsatz.module';
import { CreateRabattModule } from './create-rabatt.module';
import { CreateRatingModule } from './create-kategorie.module';
import { CreateSchlagwoerterModule } from './create-schlagwoerter.module';
import { CreateTitelModule } from './create-nachname.module';
import { CreateVerlagModule } from './create-interessen.module';
import { ErrorMessageModule } from '../../shared/error-message.module';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MaterialModule } from '../../material.module';

// Ein Modul enthaelt logisch zusammengehoerige Funktionalitaet.
// Exportierte Komponenten koennen bei einem importierenden Modul in dessen
// Komponenten innerhalb deren Templates (= HTML-Fragmente) genutzt werden.
// BuchModule ist ein "FeatureModule", das Features fuer Kunden bereitstellt
@NgModule({
    declarations: [CreateBuchComponent],
    exports: [CreateBuchComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
        CreateArtModule,
        CreateDatumModule,
        CreateIsbnModule,
        CreateLieferbarModule,
        CreatePreisModule,
        CreateRabattModule,
        CreateRatingModule,
        CreateSchlagwoerterModule,
        CreateTitelModule,
        CreateVerlagModule,
        ErrorMessageModule,
        MaterialModule,
    ],
    providers: [Title],
})
export class CreateBuchModule {}
