import { CreateSchlagwoerterComponent } from './create-schlagwoerter.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreateSchlagwoerterComponent],
    exports: [CreateSchlagwoerterComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class CreateSchlagwoerterModule {}
