import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente mit dem Tag &lt;hs-create-kategorie&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-kategorie',
    templateUrl: './create-kategorie.component.html',
})
export class CreateRatingComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly kategorie = new FormControl(undefined);

    ngOnInit() {
        console.log('CreateRatingComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('kategorie', this.kategorie);
    }
}
