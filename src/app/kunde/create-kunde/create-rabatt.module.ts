import { CommonModule } from '@angular/common';
import { CreateRabattComponent } from './create-rabatt.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreateRabattComponent],
    exports: [CreateRabattComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class CreateRabattModule {}
