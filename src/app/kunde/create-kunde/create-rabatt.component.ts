import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente mit dem Tag &lt;hs-create-rabatt&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-rabatt',
    templateUrl: './create-rabatt.component.html',
})
export class CreateRabattComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly rabatt = new FormControl(undefined, Validators.required);

    ngOnInit() {
        console.log('CreateRabattComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('rabatt', this.rabatt);
    }
}
