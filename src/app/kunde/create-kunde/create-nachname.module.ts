import { CommonModule } from '@angular/common';
import { CreateTitelComponent } from './create-nachname.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreateTitelComponent],
    exports: [CreateTitelComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class CreateTitelModule {}
