import { CommonModule } from '@angular/common';
import { CreateIsbnComponent } from './create-username.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreateIsbnComponent],
    exports: [CreateIsbnComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class CreateIsbnModule {}
