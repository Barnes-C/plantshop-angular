import {
    ActivatedRouteSnapshot,
    CanDeactivate,
    RouterStateSnapshot,
    UrlTree,
} from '@angular/router';
import { CreateBuchComponent } from './create-kunde.component';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// https://angular.io/api/router/CanDeactivate
// https://angular.io/guide/router#can-deactivate-guard

@Injectable({ providedIn: 'root' })
export class CreateBuchGuard implements CanDeactivate<CreateBuchComponent> {
    constructor() {
        console.log('CreateBuchGuard.constructor()');
    }

    canDeactivate(
        createBuch: CreateBuchComponent,
        _: ActivatedRouteSnapshot,
        __: RouterStateSnapshot,
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        if (createBuch.fertig) {
            // Seite darf zur gewuenschten URL verlassen werden
            return true;
        }

        createBuch.showWarning = true;
        createBuch.fertig = true;
        console.warn('CreateBuchGuard.canDeactivate(): Verlassen der Seite');
        return false;
    }
}
