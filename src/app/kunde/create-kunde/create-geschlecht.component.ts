import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente mit dem Tag &lt;hs-create-geschlecht&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-geschlecht',
    templateUrl: './create-geschlecht.component.html',
})
export class CreateArtComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly geschlecht = new FormControl('WEIBLICH');

    ngOnInit() {
        console.log('CreateArtComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('geschlecht', this.geschlecht);
    }
}
