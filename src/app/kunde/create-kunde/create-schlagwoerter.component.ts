import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente mit dem Tag &lt;hs-create-schlagwoerter&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-schlagwoerter',
    templateUrl: './create-schlagwoerter.component.html',
})
export class CreateSchlagwoerterComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly javascript = new FormControl(false);

    readonly typescript = new FormControl(false);

    ngOnInit() {
        console.log('CreateSchlagwoerterComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('javascript', this.javascript);
        this.form.addControl('typescript', this.typescript);
    }
}
