import { CreateArtComponent } from './create-geschlecht.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';

@NgModule({
    declarations: [CreateArtComponent],
    exports: [CreateArtComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class CreateArtModule {}
