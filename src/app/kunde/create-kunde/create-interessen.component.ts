import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente mit dem Tag &lt;hs-create-interessen&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-interessen',
    templateUrl: './create-interessen.component.html',
})
export class CreateVerlagComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly interessen = new FormControl(undefined, Validators.required);

    ngOnInit() {
        console.log('CreateVerlagComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('interessen', this.interessen);
    }
}
