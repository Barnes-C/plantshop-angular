import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import type { OnInit } from '@angular/core';

/**
 * Komponente mit dem Tag &lt;hs-create-umsatz&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-umsatz',
    templateUrl: './create-umsatz.component.html',
})
export class CreatePreisComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly umsatz = new FormControl(undefined, Validators.required);

    ngOnInit() {
        console.log('CreatePreisComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('umsatz', this.umsatz);
    }
}
