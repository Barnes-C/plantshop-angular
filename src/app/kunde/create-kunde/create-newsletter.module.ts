import { CreateLieferbarComponent } from './create-newsletter.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreateLieferbarComponent],
    exports: [CreateLieferbarComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class CreateLieferbarModule {}
