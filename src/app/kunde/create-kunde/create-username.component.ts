import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import type { OnInit } from '@angular/core';
/**
 * Komponente mit dem Tag &lt;hs-create-username&gt;, um das Erfassungsformular
 * f&uuml;r ein neues Kunde zu realisieren.
 */
@Component({
    selector: 'hs-create-username',
    templateUrl: './create-username.component.html',
})
export class CreateIsbnComponent implements OnInit {
    @Input()
    readonly form!: FormGroup;

    readonly username = new FormControl(undefined, [Validators.required]);

    ngOnInit() {
        console.log('CreateIsbnComponent.ngOnInit');
        // siehe formControlName innerhalb @Component({templateUrl: ...})
        this.form.addControl('username', this.username);
    }
}
