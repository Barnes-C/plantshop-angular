import { CommonModule } from '@angular/common';
import { CreateVerlagComponent } from './create-interessen.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreateVerlagComponent],
    exports: [CreateVerlagComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class CreateVerlagModule {}
