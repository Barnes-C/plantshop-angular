import { CreateDatumComponent } from './create-geburtsdatum.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';

@NgModule({
    declarations: [CreateDatumComponent],
    exports: [CreateDatumComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class CreateDatumModule {}
