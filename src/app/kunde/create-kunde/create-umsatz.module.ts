import { CommonModule } from '@angular/common';
import { CreatePreisComponent } from './create-umsatz.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
    declarations: [CreatePreisComponent],
    exports: [CreatePreisComponent],
    imports: [CommonModule, ReactiveFormsModule, MaterialModule],
})
export class CreatePreisModule {}
