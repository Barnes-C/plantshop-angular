import { CreateRatingComponent } from './create-kategorie.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';

@NgModule({
    declarations: [CreateRatingComponent],
    exports: [CreateRatingComponent],
    imports: [ReactiveFormsModule, MaterialModule],
})
export class CreateRatingModule {}
