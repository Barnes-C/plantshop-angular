import type { AfterViewInit, OnDestroy } from '@angular/core';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { BuchService } from '../shared';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

/**
 * Komponente mit dem Tag &lt;hs-liniendiagramm&gt; zur Visualisierung
 * von Bewertungen durch ein Liniendiagramm.
 */
@Component({
    selector: 'hs-liniendiagramm',
    templateUrl: './diagramm.html',
})
export class LiniendiagrammComponent implements AfterViewInit, OnDestroy {
    // query results available in ngAfterViewInit
    @ViewChild('chartCanvas', { static: false })
    chartCanvas!: ElementRef<HTMLCanvasElement>;

    private lineChartSubscription!: Subscription;

    constructor(
        private readonly buchService: BuchService,
        private readonly titleService: Title,
    ) {
        console.log('LiniendiagrammComponent.constructor()');
    }

    /**
     * Das Liniendiagramm beim Tag <code><canvas></code> einf&uuml;gen.
     * Erst in ngAfterViewInit kann auf ein Kind-Element aus dem Templates
     * zugegriffen werden.
     */
    ngAfterViewInit() {
        this.lineChartSubscription = this.buchService.createLinearChart(
            this.chartCanvas.nativeElement,
        );
        this.titleService.setTitle('Liniendiagramm');
    }

    ngOnDestroy() {
        this.lineChartSubscription.unsubscribe();
    }
}
