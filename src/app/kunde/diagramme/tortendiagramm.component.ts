import type { AfterViewInit, OnDestroy } from '@angular/core';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { BuchService } from '../shared';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

/**
 * Komponente mit dem Tag &lt;hs-tortendiagramm&gt; zur Visualisierung
 * von Bewertungen durch ein Tortendiagramm.
 */
@Component({
    selector: 'hs-tortendiagramm',
    templateUrl: './diagramm.html',
})
export class TortendiagrammComponent implements AfterViewInit, OnDestroy {
    // query results available in ngAfterViewInit
    @ViewChild('chartCanvas', { static: false })
    chartCanvas!: ElementRef<HTMLCanvasElement>;

    private pieChartSubscription!: Subscription;

    constructor(
        private readonly buchService: BuchService,
        private readonly titleService: Title,
    ) {
        console.log('TortendiagrammComponent.constructor()');
    }

    /**
     * Das Tortendiagramm beim Tag <code><canvas></code> einf&uuml;gen.
     * Erst in ngAfterViewInit kann auf ein Kind-Element aus dem Templates
     * zugegriffen werden.
     */
    ngAfterViewInit() {
        this.pieChartSubscription = this.buchService.createPieChart(
            this.chartCanvas.nativeElement,
        );
        this.titleService.setTitle('Tortendiagramm');
    }

    ngOnDestroy() {
        this.pieChartSubscription.unsubscribe();
    }
}
