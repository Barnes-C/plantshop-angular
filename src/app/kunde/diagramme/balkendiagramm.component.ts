import type { AfterViewInit, OnDestroy } from '@angular/core';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { BuchService } from '../shared';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

/**
 * Komponente mit dem Tag &lt;hs-balkendiagramm&gt; zur Visualisierung
 * von Bewertungen durch ein Balkendiagramm.
 * https://blog.angular-university.io/angular-viewchild
 */
@Component({
    selector: 'hs-balkendiagramm',
    templateUrl: './diagramm.html',
})
export class BalkendiagrammComponent implements AfterViewInit, OnDestroy {
    // query results available in ngAfterViewInit
    @ViewChild('chartCanvas', { static: false })
    chartCanvas!: ElementRef<HTMLCanvasElement>;

    private barChartSubscription!: Subscription;

    constructor(
        private readonly buchService: BuchService,
        private readonly titleService: Title,
    ) {
        console.log('BalkendiagrammComponent.constructor()');
    }

    /**
     * Das Balkendiagramm beim Tag <code><canvas></code> einf&uuml;gen.
     * Erst in ngAfterViewInit kann auf ein Kind-Element aus dem Templates
     * zugegriffen werden:
     * https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html.
     */
    ngAfterViewInit() {
        this.barChartSubscription = this.buchService.createBarChart(
            this.chartCanvas.nativeElement,
        );

        this.titleService.setTitle('Balkendiagramm');
    }

    ngOnDestroy() {
        this.barChartSubscription.unsubscribe();
    }
}
