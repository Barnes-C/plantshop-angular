import { BalkendiagrammModule } from './diagramme/balkendiagramm.module';
import { BuchRoutingModule } from './kunde-routing.module';
import { CreateBuchModule } from './create-kunde/create-kunde.module';
import { DetailsBuchModule } from './details-kunde/details-kunde.module';
import { LiniendiagrammModule } from './diagramme/liniendiagramm.module';
import { NgModule } from '@angular/core';
import { SucheBuecherModule } from './suche-kunden/suche-kunden.module';
import { TortendiagrammModule } from './diagramme/tortendiagramm.module';
import { UpdateBuchModule } from './update-kunde/update-kunde.module';

@NgModule({
    imports: [
        BalkendiagrammModule,
        CreateBuchModule,
        DetailsBuchModule,
        LiniendiagrammModule,
        SucheBuecherModule,
        TortendiagrammModule,
        UpdateBuchModule,
        BuchRoutingModule,
    ],
})
export class BuchModule {}
