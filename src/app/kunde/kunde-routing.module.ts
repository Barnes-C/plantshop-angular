import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../auth/admin.guard';
import { BalkendiagrammComponent } from './diagramme/balkendiagramm.component';
import { CreateBuchComponent } from './create-kunde/create-kunde.component';
import { CreateBuchGuard } from './create-kunde/create-kunde.guard';
import { DetailsBuchComponent } from './details-kunde/details-kunde.component';
import { LiniendiagrammComponent } from './diagramme/liniendiagramm.component';
import { NgModule } from '@angular/core';
import { SucheBuecherComponent } from './suche-kunden/suche-kunden.component';
import { TortendiagrammComponent } from './diagramme/tortendiagramm.component';
import { UpdateBuchComponent } from './update-kunde/update-kunde.component';

// Route-Definitionen fuer das Feature-Modul "kunde":
// Zuordnung von Pfaden und Komponenten mit HTML-Templates
const routes: Routes = [
    {
        path: 'suche',
        component: SucheBuecherComponent,
    },
    {
        path: 'create',
        component: CreateBuchComponent,
        canActivate: [AdminGuard],
        canDeactivate: [CreateBuchGuard],
    },
    {
        path: 'balkendiagramm',
        component: BalkendiagrammComponent,
        canActivate: [AdminGuard],
    },
    {
        path: 'liniendiagramm',
        component: LiniendiagrammComponent,
        canActivate: [AdminGuard],
    },
    {
        path: 'tortendiagramm',
        component: TortendiagrammComponent,
        canActivate: [AdminGuard],
    },

    // id als Pfad-Parameter
    {
        path: ':id',
        component: DetailsBuchComponent,
    },
    {
        path: ':id/update',
        component: UpdateBuchComponent,
        canActivate: [AdminGuard],
    },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)],
})
export class BuchRoutingModule {}
