export const host = 'localhost';
export const port = 443;
export const browser = 'chrome';

export const dir = {
    src: 'src',
    dist: 'dist/acme',
    nginx: 'C:/Zimmermann/nginx',
    webserverConfig: 'config/webserver',
};
